```plantuml
@startmindmap
title Computadoras Electrónicas
*[#145A32] Computadoras
**[#1E8449] Electromecanicas
***[#229954] Harvard Mark I
****[#27AE60] Construida por IBM en 1944 
*****[#58D68D] Contenia\n765000 componentes\n80 km cable\nMotor de 5 hp
******[#7DCEA0] Utilizada en el proyecto\nManhattan
******[#7DCEA0] Realizaba\n3 sumas o restas por segundo\nMultiplicacion en 6 segundos\nDivision 15 segundos\nOperaciones complejas 1 minuto o mas
***[#229954] Usaban dispositivos Relé
****[#27AE60] Se compone de\nBobina\nBrazo de hierro movil\nContactos
****[#27AE60] Un buen relé \nen 1940
*****[#58D68D] Cambiama de estados\n50 veces por segundo

**[#1E8449] Electronicas
***[#229954] Valvula Termoionica
****[#27AE60] Desarrollada por\nJohn A,brose Fleming\n1904
*****[#58D68D] se compone
******[#7DCEA0] Bulbo de cristal
******[#7DCEA0] Filamento
******[#7DCEA0] Anodo
******[#7DCEA0] Catodo
****[#27AE60] Lee de Forest agrego\nun electrodo de control\nen 1906
****[#27AE60] Cambia de estado 1000 veces por segundo
****[#27AE60] Base para la creacion de la radio,\n telefono a larga distancia.
***[#229954] Colossus Mark I 
****[#27AE60] Contenia 1600 tubos de vacio
****[#27AE60] Desarrollada por Tomi Flowers\en 1943
****[#27AE60] Decodificaba comunicaciones\n nazis
****[#27AE60] Primera computadora \nElectronica programable
***[#229954] ENIAC 
****[#27AE60] Construida en 1946
*****[#58D68D] Por John Mauchly y J. Presper Eckert 
*****[#58D68D] Primera computadora programable de \nproposito GenericDisplay
*****[#58D68D] Podia realizar 5000 sumas o restas\n de 10 digitos en segundos
***[#229954] Bell Laboratories\n1947
****[#27AE60] John Bardeen\nWalter Brattaim\nWilliam Shockley
*****[#58D68D] Desarrollaron el transistor 
******[#7DCEA0] Puede cambiar de estado\n10000 veces por segundo
******[#7DCEA0] Se construyo la IBM608\nen 1957
******[#7DCEA0] Inicia la nueva era de la computacion
@endmindmap

```
```plantuml
@startmindmap
title Arquitectura Von Neumann y Arquitectura Harvard
*[#a45411] Arquitectura Von Neumann \n\t\t\ty \nArquitectura Harvard
**[#ff8000] Ley de Moore
***[#ff963e] Establece que la velocidad del procesador \no el poder de procesamiento total de las computadoras \nse duplica cada doce meses
****[#ffab66] Electrónica
*****[#ffc08c] El número de transistores \npor chip se duplica cada año
*****[#ffc08c] El costo del chip permanece sin cambios
*****[#ffc08c] Cada 18 meses se duplica la potencia\nde calculosin modificar el costo
****[#ffab66] Performance
*****[#ffc08c] Se incrementa la velocidad del procesador
*****[#ffc08c] Se incrementa la capacidad de la memoria
*****[#ffc08c] La velocidad de la memoria corre siempre por detrás de la \nvelocidad del procesador

**[#ff8000] Funcionamiento de una Computadora
***[#ff963e] Antes
****[#ffab66]  Habia sistemas cableados
****[#ffab66]  Programacion mendiante Hardware
*****[#ffc08c] Como por ejemplo en la\npelicula El Codigo Enigma
******[#ffd5b1] Se desconectaban y conectaban cosas para\n asi lograr programar la computadora
****[#ffab66]  Entrada de datos
****[#ffab66]  Tenia una secuencia de funciones\naritmeticas / logicas
****[#ffab66]  Y despues de todo lo anterior se obtenian los resultados
***[#ff963e] Ahora
****[#ffab66]  Programacion mediante Software
****[#ffab66] Interprete de instrucciones 
****[#ffab66] Señales de control
****[#ffab66] Entrada de datos
****[#ffab66] Secuencia de funciones logicas
****[#ffab66] Resultados eficientes y rapidos

**[#ff8000] Arquitectura
***[#ff963e] Von Neumann
****[#ffab66] Partes fundamentales 
*****[#ffc08c] 3 Componentes
******[#ffd5b1] Cpu \n(Unidad de Procesamiento Central)
******[#ffd5b1] Memoria principal
******[#ffd5b1] Modulo de E/S \n(Entrada salida)
*****[#ffc08c] Interconectado mediante un sistema de bus
******[#ffd5b1] Control
******[#ffd5b1] Direcciones
******[#ffd5b1] Datos e instrucciones 
****[#ffab66] Modelo
*****[#ffc08c] Es una Arquitectura basada en la descrita\n1945
******[#ffd5b1] Primer borrador de un informe\nsobre el EDVAC
*****[#ffc08c] Los datos y programas se almacenan en
******[#ffd5b1] Una misma memoria de lectura/escritura
*****[#ffc08c] Los contenidos de esta memoria se acceden
******[#ffd5b1] Indicando su posicion sin importar su tipo
*****[#ffc08c] Ejecucion en secuencia 
******[#ffd5b1] Salvo que se indique lo contrario
*****[#ffc08c] Representacion binaria
****[#ffab66] Contiene
*****[#ffc08c] Este describe una arquitectura de diseño
******[#ffd5b1] Para una computadora digital electronica \ncon partes que constan de
*******[#ffead8] Unidad central de procesamiento Cpu\nla cual contiene una unidad\de 
********[#ffffff] Control
********[#ffffff] Aritmetica Logica
********[#ffffff] Registros
*******[#ffead8] Memoria principal
********[#ffffff] La cual puede almacenar tanto instrucciones\ncomo datos
*******[#ffead8] Sistema de entrada y salida
****[#ffab66] Con este modelo surge el concepto de\nprogramaalmacenado
*****[#ffc08c] Por el cual se les conoce a las computadoras del mismo tipo
****[#ffab66] La separacion de la\nmemoria y la cpu
*****[#ffc08c] Acarreo un problema denominado \nNeumann bottleneck
****[#ffab66] Interconexion
*****[#ffc08c] Todos los componentes se comunican\natravez de un sistema de buses que se\ndivide en 
******[#ffd5b1] Bus de datos
******[#ffd5b1] Bus de direcciones
******[#ffd5b1] Bus de control
****[#ffab66] Instrucciones
*****[#ffc08c] La funcion de una computadora es \nla ejecucion de programas
******[#ffd5b1] Programas localizados en memoria
*****[#ffc08c] La CPU es quien se encarga de ejecutar\n las instrucciones
******[#ffd5b1] Atravez de un ciclo de ejecucion
*******[#ffead8] UC obtiene la instruccion \nen memoria
*******[#ffead8] Se incrementa el pc
*******[#ffead8] La instruccion de decodifica
*******[#ffead8] Se obtienen los operandos requeridos
*******[#ffead8] Se ejecuta y se dejan los resultados en memoria
*****[#ffc08c] Se emplean lenguajes ensamblador o de programacion
******[#ffd5b1] Las instrucciones las ejecuta la CPU a grandes velocidades
*****[#ffc08c] Una CPU de 3.5GHz 
******[#ffd5b1] Realiza 3000000000\noperaciones por segundo
*****[#ffc08c] Ciclo de una instruccion
******[#ffd5b1] Calculo de la direccion \nde la instruccion
******[#ffd5b1] Captacion de instruccion
******[#ffd5b1] Decodificacion de la operacion\nde la instruccion
******[#ffd5b1] Calculo de la direccion del\noperando
******[#ffd5b1] Captacion del operando
******[#ffd5b1] Operacion con datos 
******[#ffd5b1] Calculo de la operacion del operando
******[#ffd5b1] Almacenamiento del operando 

***[#ff963e] Harvard
****[#ffab66] Modelo
*****[#ffc08c] Originalmente se referia a las
******[#ffd5b1] Arquitecturas que utilizaban dispositivos de almacenamiento\nfisicamente separadas 
****[#ffab66] Contiene
*****[#ffc08c] Diseño para una computadora digital \nque consta de
******[#ffd5b1] CPU
******[#ffd5b1] Memoria principal
******[#ffd5b1] Sistema E/S
****[#ffab66] Memorias 
*****[#ffc08c] Memorias mucho mas rapidas
*****[#ffc08c] Precio muy alto
*****[#ffc08c] Nace la memoria cache
*****[#ffc08c] Mucho mejor rendimiento
****[#ffab66] Solucion Harvard
*****[#ffc08c] Almacenamiento en caches separadas
*****[#ffc08c] Se utiliza en PICs o \nMicrocontroladores
******[#ffd5b1] Usados habitualmente en electrodomesticos
*******[#ffead8] Procesamiento de audio
*******[#ffead8] Procesamiento de video
*******[#ffead8] Entre otras cosas
****[#ffab66] Procesador
*****[#ffc08c] Dos unidades principales
******[#ffd5b1] Unidad de control UC
******[#ffd5b1] Unidad aritmetica y logica ALU

****[#ffab66] Memoria de instrucciones
*****[#ffc08c] Donde se alojan las instrucciones del programa 
******[#ffd5b1] Implementa memorias no volatiles
*******[#ffead8] ROM 
*******[#ffead8] PROM
*******[#ffead8] EPROM
*******[#ffead8] EEPROM
*******[#ffead8] FLASH
****[#ffab66] Memoria de datos
*****[#ffc08c] Se almacenan datos utilizados por programas
*****[#ffc08c] Utiliza memoria RAM
@endmindmap

```

```plantuml
@startmindmap
title Basura Electrónica
*[#a41c54] Basura Electrónica
**[#ff0080] e-end
***[#ff5594] Centro de reciclaje electronico en el  \ncual montañas de basura electronica es procesada
****[#ff7ea8] Se extrae 
*****[#ffa0bd] Oro
*****[#ffa0bd] Plata
*****[#ffa0bd] Cobalto
*****[#ffa0bd] Cobre
***[#ff5594] La basura que no se recicla correctamente 
****[#ff7ea8] Es exportada a paises de 3er mundo
*****[#ffa0bd] Donde utilizan metodos peligrosos con impactos nefastos \npara el medio ambiente
**[#ff0080] RAEE
***[#ff5594] Residuos de aparatos electricos o electronicos
***[#ff5594] Cuando un electrodomestico o aparato digital\n culmina su vida util se confierte en un RAEE
****[#ff7ea8] Por ejemplo:
*****[#ffa0bd] Celulares
*****[#ffa0bd] Computadoras
*****[#ffa0bd] Televisores
*****[#ffa0bd] Radios
*****[#ffa0bd] Licuadoras
*****[#ffa0bd] Lavadoras
***[#ff5594] Proceso 
****[#ff7ea8] Recoleccion 
****[#ff7ea8] Separacion segun su categoria 
****[#ff7ea8] Desarmar
****[#ff7ea8] Recuperar los metales
***[#ff5594] El usar metales reciclados
****[#ff7ea8] Reduce considerablemente la contaminacion
***[#ff5594] Metales, plastico y otros compuestos
****[#ff7ea8] Que deben ser tratados adecuadamente
***[#ff5594] El robo de datos
****[#ff7ea8] Existen personas que se dedican al robo de datos\n mediante la basura electronica
****[#ff7ea8] O la reparacion con fines de robo de datos
***[#ff5594] Existen centros de reciclaje
****[#ff7ea8] e-end
*****[#ffa0bd] Estados unidos
****[#ff7ea8] Green Recycling
*****[#ffa0bd] Peru
******[#ffc1d3] Estima 400 000 toneladas de basura electronica
****[#ff7ea8] Techemet
*****[#ffa0bd] Mexico
******[#ffc1d3] Desecha 150 000 toneladas de basura electronica
****[#ff7ea8] Servicios Ecologico
*****[#ffa0bd] Costa Rica
****[#ff7ea8] Remsa
***[#ff5594] Graves problemas ambientales
****[#ff7ea8] Contaminacion del agua y aire 
*****[#ffa0bd] La mala practica conlleva a contaminar
******[#ffc1d3] La contaminacion enferma al ser humano y provoca\nenfermedades por las toxinas
***[#ff5594] El beneficio de reciclar
****[#ff7ea8] Reduce la contaminacion
****[#ff7ea8] Aprovecha metales desechados
****[#ff7ea8] Obtencion de materias primas
*****[#ffa0bd] como por ejemplo
******[#ffc1d3] plastico
******[#ffc1d3] Vidrio
******[#ffc1d3] Metales ferrosos y no ferrosos
******[#ffc1d3] electronica
*****[#ffa0bd] Se producen productos como:
******[#ffc1d3] Suela de zapato
******[#ffc1d3] Tapas de rines de coches
******[#ffc1d3] Adornos
******[#ffc1d3] Piso
******[#ffc1d3] Adoquin
@endmindmap

```
